const start = () => {
    let nest = document.getElementById('nest')
    let egg = document.getElementById('egg')
    let bird = document.getElementById('bird');
    let wrinkles = document.getElementById('wrinkles');
    nest.style.transition = "opacity 1s"
    nest.style.opacity = "1"
    egg.style.transition = "opacity 1s"
    egg.style.opacity = "1"
    bird.style.transition = "opacity 1s"
    bird.style.opacity = "1"
    wrinkles.style.transition = "opacity 1s"
    wrinkles.style.opacity = "1"

}
const shakeBird = () => {
    let bird = document.getElementById('bird');
    bird.classList.remove('shake-bird')
    bird.classList.add('shake-bird')
}
const shakeNest = () => {
    let nest = document.getElementById('nest')
    let egg = document.getElementById('egg')
    let bird = document.getElementById('bird');
    bird.classList.remove('shake-bird')
    bird.classList.add('shake-bird')
    egg.classList.remove('shake-bird')
    egg.classList.add('shake-bird')
    nest.classList.remove('shake-bird')
    nest.classList.add('shake-bird')

}
const moveEgg = () => {
    let egg = document.getElementById('egg')
    egg.classList.remove('move-egg')
    egg.classList.add('move-egg')

}
const fallLeaves = () => {
    let leaves = document.getElementById('leaves')

    let leaf = document.getElementsByTagName('i')
    for (let index = 0; index < leaf.length; index++) {
        leaf.item(index).remove();
    }
    for (let index = 0; index < 14; index++) {
        let i = document.createElement('i')
        leaves.appendChild(i)
    }
    leaves.classList.remove('falling-leaves')
    leaves.classList.add('falling-leaves')
}